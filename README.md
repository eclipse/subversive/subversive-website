# Eclipse Subversive

Learn to how [contributing](CONTRIBUTING.md).

Explore the [security](SECURITY.md) question.

Use the [Community standarts](CODE_OF_CONDUCT.MD).

Look to the [EPL v2.0](LICENSE). 

Read the [NOTICE](NOTICE.md).

Copyright © 2007, 2025 Contributors to the Eclipse Foundation.

[![Eclipse License](https://img.shields.io/badge/License-EPL--2.0-thistle.svg)](https://gitlab.eclipse.org/eclipse/subversive/subversive/-/blob/main/LICENSE). 

See [Eclipse Subversive README](https://gitlab.eclipse.org/eclipse/subversive/subversive/-/blob/main/README.md).